import { TestBed } from '@angular/core/testing';

import { DataProdcService } from './data-prodc.service';

describe('DataProdcService', () => {
  let service: DataProdcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataProdcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
