const baseUrl = 'https://h6li2mrz2i.execute-api.us-east-2.amazonaws.com/beta';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlphaService {
  private token;

  constructor(private http: HttpClient) {}

  private request(method: string, url: string, data?: any,response_type:"json"|"text"|"arraybuffer" = 'json') {
    const result = this.http.request(method, url, {
      body: data,
      responseType: response_type,
      observe: 'body'
    });
    return new Promise<any>((resolve, reject) => {
      result.subscribe(resolve as any, reject as any);
    });
  }
  
  logout(){
    this.request('get',`${baseUrl}/logout/${this.token}`).then(()=>{
      this.token = 0;
    });
  }
  
  public async LoadHome(code?){
    if(!code){
      if(this.token){
        if(this.token > 0){
          return await this.main('get-home',{module:'comerciante'});
        }
      }
    }
    code = new URLSearchParams(code);
    code = code.get('token')
    code = this.decode(code,'|');
    if(!code){
      window.location.href='https://usac-ccv.gitlab.io/portal';
      return;
    }
    this.token = code.token;
    console.log(code);
    return await this.main('get-home',{module: 'comerciante'});
  }

  public main(tipo: string, data?: object): any {
    let req = {
        token : this.token,
        tipo: tipo,
        data: data
    };
    
    return new Promise((resolve,reject)=>{
      this.request('post', `${baseUrl}/main`,req).then((v)=>resolve(v)).catch((err)=>{
        console.log(err);
        reject({tipo:tipo,data:data,error:err});
      })
    });
  }

  resultadoSolicitud(correo,mensaje,subject){
    let post = {
      token:Number(this.token),
      to: correo,
      body: mensaje,
      subject: subject
    };
    return this.request('post',`${baseUrl}/correo`,post);
  }

  /** Utils **/
  private decode (str,key){
    str = decodeURIComponent(decodeURIComponent(str)
        .split(key).map(
                c=>String.fromCharCode(Number(c))
        ).join(''));
    try { return JSON.parse(str) }
    catch (err) {
      return null;
    }
  }

  success(message:string){
    if(!message)return;
    alert(message);
  }
  inform(message:any){
    if(!message)return;
    alert(message);
  }
  error(message:any){
    if(!message)return;
    alert(message);
  }

  async registerFile(fbody : object) {
    return await this.request('post',`${baseUrl}/files`,fbody);
  }


}
