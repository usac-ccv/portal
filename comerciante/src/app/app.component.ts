import { Component } from '@angular/core';
import { AlphaService } from './Services/alpha.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'comerciante';
  constructor(public Alpha:AlphaService){
    //Recompile comerciante module
    this.reload();
  }
  async reload(){
    const initial_data =
      await this.Alpha.LoadHome(window.location.search);
    console.log(initial_data);
  }

  cerrarSesion(){
      window.location.href='https://usac-ccv.gitlab.io/portal';
      return;
  }
}
