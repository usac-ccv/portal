import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { InicioComercianteComponent } from './inicio-comerciante/inicio-comerciante.component';
import { UpdateGeneralInfoComponent } from './update-general-info/update-general-info.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { AddProductoComponent } from './add-producto/add-producto.component';
import { FormsModule } from '@angular/forms';
import { UpdateProductoComponent } from './update-producto/update-producto.component';
import { GridComponent } from './grid/grid.component';
import { CardTiendaComponent } from './card-tienda/card-tienda.component';
import { ReportesComponent } from './reportes/reportes.component';
import { ProductosVendidosComponent } from './productos-vendidos/productos-vendidos.component';
import { ChartsModule } from 'ng2-charts';
import { CategoriaVendidaComponent } from './categoria-vendida/categoria-vendida.component';
import { ClienteFrecuenteComponent } from './cliente-frecuente/cliente-frecuente.component';
import { VentasMesComponent } from './ventas-mes/ventas-mes.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComercianteComponent,
    UpdateGeneralInfoComponent,
    FileUploadComponent,
    GridComponent,
    CardTiendaComponent,
    AddProductoComponent,
    UpdateProductoComponent,
    ReportesComponent,
    ProductosVendidosComponent,
    CategoriaVendidaComponent,
    ClienteFrecuenteComponent,
    VentasMesComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
