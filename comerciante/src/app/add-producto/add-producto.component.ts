import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-add-producto',
  templateUrl: './add-producto.component.html',
  styleUrls: ['./add-producto.component.css']
})
export class AddProductoComponent implements OnInit {

  constructor(public Alpha:AlphaService, public router: Router) { }

  ngOnInit(): void {
  }


  public producto = {
    nombre: '',
    precio: 0,
    cantidad: 0
  }


  async add_Producto() {
    await this.Alpha.main('crear-producto', this.producto);
    this.salir();
  }

  clean_campos(){
    this.producto = {
      nombre: '',
      precio: 0,
      cantidad: 0
    }
  }

  salir(){
    this.clean_campos();
    this.router.navigate(['']);
  }
}
