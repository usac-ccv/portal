import { Component, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-cliente-frecuente',
  templateUrl: './cliente-frecuente.component.html',
  styleUrls: ['./cliente-frecuente.component.css']
})
export class ClienteFrecuenteComponent implements OnInit {

  public totalPedidosOptions={
    scaleShowVerticalLines: false,
    resposnive: false,
    position: 'right'
  };

  public totalPedidosColor: Color[] = [
    { // Euro - Azul
      backgroundColor: 'rgba(153,255,0)',
      borderColor: 'rgba(0,0,255,1)',
      pointBackgroundColor: 'rgba(0,0,255,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(0,0,255,1)'
    }
  ];

  public totalPedidosLabel:string[] = ['Mas vendido'];
  public totalPedidosType= 'bar';
  public totalPedidosLegend= true;
  public dataX;
  public dataY;
  informacion: [];
  totalPedidosData=[];
  ChartData: ChartDataSets[];
  ChartLabel: Label[];

  data = []; 
  labels = []; 

  constructor(public Alpha:AlphaService) { 
    this.data= [];
    this.labels= [];
    this.ChartLabel=  [];
    this.ChartData = [{ data: [], label: '' }]; 
    this.getInformacion();
  }

  ngOnInit(): void {
  }

  async getInformacion(){
    this.informacion = await this.Alpha.main('get-stats');
    this.dataX= Object.values(Object.values(this.informacion)[0])[0];
    this.dataY= Object.values(Object.values(this.informacion)[0])[1];
    console.log(this.informacion)
    
    for(let index in Object(this.dataX)){
      this.data.push(Object(this.dataX)[index]);
    }

    for(let index in Object(this.dataY)){
      this.labels.push(Object(this.dataY)[index]);
    }

    let clone = JSON.parse(JSON.stringify(this.data));
    let clone1 = JSON.parse(JSON.stringify(this.labels));
    this.ChartLabel= clone;
    this.ChartData = [{ data: clone1, label: 'Numero de compras' }];
    console.log(this.informacion)
  }

}
