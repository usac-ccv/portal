import { Component, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-update-general-info',
  templateUrl: './update-general-info.component.html',
  styleUrls: ['./update-general-info.component.css']
})
export class UpdateGeneralInfoComponent implements OnInit {

  constructor(public alpha: AlphaService) { 
    
  }

  public image: string;


  actualizado = false;
  error = false;
  
  tnombre = '';
  tslogan = '';
  tdesc = '';

  tienda: {
    id:number,
    nombre: string,
    descripcion: string,
    slogan: string,
    logo: string, //logo is an String with an image url.
    categorias: string[],
    departamentos: string[],
    productos: Array<{
      id: number,
      nombre: string,
      imagen: string, //image url 
      cantidad: number,
      precio: number
    }>
  };

  ngOnInit(): void {
    setInterval(()=>{
      this.actualizado = false;
    },5000);
    this.getInfo();
  }

  async getInfo(){
    const info = await this.alpha.main('get-tienda');
    this.tienda = info;
    this.tnombre = info.nombre;
    this.tslogan = info.slogan;
    this.tdesc = info.descripcion;
    this.image = this.tienda.logo;
  }

  
  async actualizar(nombre,slogan,desc,logo){
    if (nombre != '') {
      this.tnombre = nombre;
    }
    if (slogan != '') {
      this.tslogan = slogan;
    }
    if (desc != '') {
      this.tdesc = desc;
    }
    if(logo != ''){
      this.image = logo;
    }
    console.log('envio'+this.tnombre+'=>'+this.tslogan+'=>'+this.tdesc+'=>'+this.image);
    var result = await this.alpha.main('update-tienda',{nombre:this.tnombre,slogan:this.tslogan,descripcion:this.tdesc,logo:this.image});
    console.log(result);
    if(result.success){
      this.actualizado = true;
    }else{
      this.error = true;
    }

  }

  refresh($event: {id:number, url:string}){
    this.actualizar('','','',$event.id);
    this.image = $event.url;
    console.log($event.url);
    console.log('imagen con id: '+$event.id);
    this.getInfo();
  }

}
