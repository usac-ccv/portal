import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTiendaComponent } from './card-tienda.component';

describe('CardTiendaComponent', () => {
  let component: CardTiendaComponent;
  let fixture: ComponentFixture<CardTiendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardTiendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTiendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
