import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriaVendidaComponent } from './categoria-vendida.component';

describe('CategoriaVendidaComponent', () => {
  let component: CategoriaVendidaComponent;
  let fixture: ComponentFixture<CategoriaVendidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriaVendidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriaVendidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
