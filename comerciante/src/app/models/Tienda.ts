import { Product } from './Product'
export class Tienda {
    nombre: string;
    logo: string; 
    id ?: number;
    descripcion?: string;
    slogan?: string;
    categorias?: string[];
    departamentos?: string[];
    productos?: Array<Product>;
}
