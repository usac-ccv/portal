import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioComercianteComponent } from './inicio-comerciante.component';

describe('InicioComercianteComponent', () => {
  let component: InicioComercianteComponent;
  let fixture: ComponentFixture<InicioComercianteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InicioComercianteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioComercianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
