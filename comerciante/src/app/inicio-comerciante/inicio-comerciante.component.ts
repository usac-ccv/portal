import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { Tienda } from '../models/Tienda'
import { Product } from '../models/Product'
import { AlphaService } from '../Services/alpha.service';
import { DataProdcService } from "../Services/data-prodc.service";

@Component({
  selector: 'app-inicio-comerciante',
  templateUrl: './inicio-comerciante.component.html',
  styleUrls: ['./inicio-comerciante.component.css']
})
export class InicioComercianteComponent implements OnInit {

  productos: Array<Product>;
  tienda: Tienda;

  constructor(public Alpha: AlphaService, public router: Router, public set_dataprod:DataProdcService) { }

  ngOnInit(): void {
    this.productos=[];
    this.LoadHome();
  }

  async LoadHome(){
    const home = await this.Alpha.LoadHome();
    this.productos = home.productos;
    this.tienda = home;
  }

  async eliminarProducto(id: number){
    await this.Alpha.main("eliminar-producto", { id: id });
    this.productos=[];
    this.LoadHome();
  }

  call_updateprod(data_pruct:Product){
    this.set_dataprod.product = data_pruct;
    this.set_dataprod.departamentos = this.tienda.departamentos;
    this.router.navigate(['update-prod']);
  }
}
