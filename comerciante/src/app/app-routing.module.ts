import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProductoComponent } from './add-producto/add-producto.component';
import { InicioComercianteComponent } from './inicio-comerciante/inicio-comerciante.component';
import { UpdateGeneralInfoComponent } from './update-general-info/update-general-info.component';
import { UpdateProductoComponent } from './update-producto/update-producto.component';
import { ReportesComponent } from './reportes/reportes.component';
import { ProductosVendidosComponent } from './productos-vendidos/productos-vendidos.component';
import { CategoriaVendidaComponent } from './categoria-vendida/categoria-vendida.component';
import { ClienteFrecuenteComponent } from './cliente-frecuente/cliente-frecuente.component';
import { VentasMesComponent } from './ventas-mes/ventas-mes.component';

const routes: Routes = [
  {path: 'index.html', component: InicioComercianteComponent},
  {path: 'updateInfo', component: UpdateGeneralInfoComponent},
  {path: '', component: InicioComercianteComponent},
  {path: 'add-prod', component: AddProductoComponent},
  {path: 'update-prod', component: UpdateProductoComponent},
  {path: 'reportes', component: ReportesComponent},
  {path: 'producto-vendido', component: ProductosVendidosComponent},
  {path: 'categoria-vendida', component: CategoriaVendidaComponent},
  {path: 'clientes-frecuente', component: ClienteFrecuenteComponent},
  {path: 'venta-mes', component: VentasMesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
