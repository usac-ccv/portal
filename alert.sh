git add .
JK="$(git branch --show-current)"
git commit -m "$1"
git push -o merge_request.create -o \
	merge_request.target=develop origin "$JK"
