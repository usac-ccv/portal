# Centro Comercial Virtual (CCV)
El centro comercial virtual (CCV) es un comercio en linea que reune
la oferta de varios comercios dentro de un solo sitio donde el 
cliente puede moverse entre las distintas tiendas como si se 
encontraraen una sola filtrando los resultados por categoria y 
manteniendo un solo carro de compra para realizar un unico pago
con tarjeta de credito o debito.

# Release
El lanzamiento oficial de la primer version del portal CCV
ya se encuentra disponible! <br>
Puedes acceder a ella entrando al siguiente link: 
https://usac-ccv.gitlab.io/portal
<br>
Gracias por visitarnos y esperamos tengas una excelente experiencia!

# Canary release
El equipo de desarrollo toma muy enserio la calidad del codigo que
publica, por lo que todos los lanzamientos oficiales son 
analizados y probados detenidamente antes de su publicación.
En consecuencia, las updates al portal tienden a ser publicadas
con una lentitud relativa. Debido a esto hemos abierto un 
segundo portal catalogado como Canary release. Cambios a este
portal son publicados con mucha más frecuencia que el oficial, 
debido a que este portal es el que nos sirve a nosotros como 
desarrolladores llevar a cabo las pruebas, detectar bugs y arreglarlos
antes de realizar cualquier publicación oficial.
Si estas interesado en esta versión o te gustaría ayudarnos a probar
los últimos cambios al portal y reportar bugs o solicitudes, puedes
entrar a esta versión publica y emplearlo. Si encuentras un bug
en esta versión no dudes en abrir una issue y lo arreglaremos lo 
antes posible. Finalmente, se recuerda que esta versión recibe todos
los cambios antes que la oficial y que una vez se alcance un nivel
de estabilidad deseado para ser publicado se envía a la oficial. Por
tanto ambas versiones están sincronizadas inmediatamente después
de un despliegue oficial.
Puedes entrar a este portal en el siguiente link:
https://renato776.github.io/ccv
<br>
Si deseas ver el repositorio para enviarnos una issue official sobre
algun bug en el sistema, puedes enviarlo entrando al repositorio:
https://github.com/Renato776/ccv
<br>
<br>
Gracias por escogernos y esperamos tenga una gran experiencia en 
el Centro Comercial Virtual!

# Scrum meetings 
1. https://drive.google.com/file/d/13sl_IPkQNTLjRF7HwMEECwLbdaHJ6N6R/view?usp=sharing
2. https://drive.google.com/file/d/1X4ENGgaJIsOVgdVI0-mEhLggfy7Sog1X/view?usp=sharing
3. https://drive.google.com/file/d/16_w92aCoDahmU1i_aCd3Ik2edfBM4qzU/view?usp=sharing
4. https://drive.google.com/file/d/1Kzb906K1vzaM8h7e4NZsYWykd_JfeuFf/view?usp=sharing
5. https://drive.google.com/file/d/17uKJC1oQpN73nWDlW3dPQVV0B-FE4Oog/view?usp=sharing
6. https://drive.google.com/file/d/1EpYsJFrRwp6n_qTQoWqFrwbfYPCG9FeQ/view?usp=sharing

# SCRM fase 3
https://drive.google.com/drive/folders/1mxEeiComK1OR6VVyt-dEUVj2-PAUJMZM?usp=sharing
