#!/bin/env sh
git checkout develop
git fetch origin
git reset --hard origin/develop
git checkout -b "$1"
echo "Entorno listo para desarrollo. Trabajando en rama:
$1
Puedes usar el comando: ./commit.sh MESSAGE cada vez que se 
realize un avance, sin importar la importancia del avance.
Cuando la feature que se esta trabajando este lista para 
enviarla a produccion, utilizar el comando: ./alert.sh"
