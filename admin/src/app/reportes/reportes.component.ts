import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  informacion: [];
  public valor;
  @ViewChild('ipId') span: ElementRef;

  constructor(public Alpha:AlphaService) { 
    this.getInformacion();
  }

  ngOnInit(): void {
  }

  async getInformacion(){
    this.informacion = await this.Alpha.main('get-stats');
    this.valor= Object.values(this.informacion)[0];
    this.span.nativeElement.innerHTML= this.valor;
  }
}
