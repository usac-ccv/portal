import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { AlphaService } from '../Services/alpha.service';
import { DataProdcService } from '../Services/data-prodc.service';
import { Product } from '../models/Product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tienda-display',
  templateUrl: './tienda-display.component.html',
  styleUrls: ['./tienda-display.component.css']
})
export class TiendaDisplayComponent implements OnInit {

  constructor(public Alpha: AlphaService, public set_dataprod:DataProdcService, private router: Router) {}

  ngOnInit(): void {
    this.Alpha.ReloadTienda();
    this.getDepartamentos();
  }

  async delete_producto(id: number){
    await this.Alpha.main("eliminar-producto", { id: id });
    this.Alpha.ReloadTienda();
  }

  update_producto(data_pruct:Product){
    this.set_dataprod.product = data_pruct;
    this.set_dataprod.departamentos = this.Alpha.tienda.departamentos;
    this.router.navigate(['update-prod']);
  }

  getSelectedValue(value: any) {
    console.log('Selected value:', value);
  }


  departamentos = [];
  async getDepartamentos(){
    this.departamentos = [];
    this.departamentos = await this.Alpha.main('get-departamentos')
  }

 selected: string = ' ';

 selectOption(id: string) {
 }

 async enlazar(){
   const result = await this.Alpha.main('enlazar-departamento',{departamento: this.selected});
   console.log(result)
 }

}
