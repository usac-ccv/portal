import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiendaDisplayComponent } from './tienda-display.component';

describe('TiendaDisplayComponent', () => {
  let component: TiendaDisplayComponent;
  let fixture: ComponentFixture<TiendaDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TiendaDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TiendaDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
