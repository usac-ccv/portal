import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tienda } from '../models/Tienda';
import { Product } from '../models/Product';
import { AlphaService } from '../Services/alpha.service';
import { DataProdcService } from "../Services/data-prodc.service";

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['./administracion.component.css']
})
export class AdministracionComponent implements OnInit {

  tiendas: Array<Tienda>;

  constructor(public Alpha: AlphaService, public router: Router, public set_dataprod:DataProdcService) { }

  ngOnInit(): void {
    this.tiendas=[];
    this.LoadHome();
  }

  async LoadHome(){
    const home = await this.Alpha.LoadHome();
    this.Alpha.productos = home.productos;
    this.tiendas = home.tiendas;
  }

  select_tienda(nombre: string){
    this.Alpha.tienda = new Tienda();
    this.Alpha.tienda.nombre = nombre;
    this.router.navigate(['tienda-display']);
  }

  async update_product(data_pruct:Product){
    this.set_dataprod.product = data_pruct;
    const t = await this.Alpha.main('get-tienda',{ id: data_pruct.tienda });
    this.set_dataprod.departamentos = t.departamentos;
    this.router.navigate(['update-prod']);
  }

  async eliminar_product(id: number){
    await this.Alpha.main("eliminar-producto", { id: id });
    this.LoadHome();
  }
}
