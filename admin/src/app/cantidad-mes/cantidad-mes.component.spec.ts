import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CantidadMesComponent } from './cantidad-mes.component';

describe('CantidadMesComponent', () => {
  let component: CantidadMesComponent;
  let fixture: ComponentFixture<CantidadMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CantidadMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CantidadMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
