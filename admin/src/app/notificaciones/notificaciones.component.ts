import { Component, OnInit } from '@angular/core';
import { servSolicitudes } from '../Services/solicitudes-service.service';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.css']
})

export class NotificacionesComponent implements OnInit{

  constructor(private servSolicitudes: servSolicitudes, public Alpha:AlphaService){
    this.traer_solicitudes();
  }

  ngOnInit(): void{
  }

  random(max){
    return Math.floor(Math.random() * (max + 1));
  }

  getCredenciales(nombre){  
    let base:string = this.random(999).toString();

    let user = "";
    let pass = this.random(9999).toString();

    if(nombre.length > 3){
      user = user + nombre.charAt(0) + nombre.charAt(1) + nombre.charAt(2) + base;
      pass = pass + nombre.charAt(2) + nombre.charAt(1) + nombre.charAt(0);
    }else{
      user = user + 'xyz' + base;
      pass = pass + 'zxy';
    }
    return {u:user, p:pass}
  }
   
  async enviar(resultado,email, nombre_tienda, usuario, descrip, telefono){
    if(resultado == 1){
      let info = this.getCredenciales(email);
      //INFO CONTIENE LAS CREDENCIALES NUEVAS info.u nombre de usuario, info.p la password, email el corro
      const resultado = await this.Alpha.enviarCorreo(email,`Gracias por registarte con CCV!\nTu solicitud ha sido aceptada.\nTu nuevo usuario: ${info.u}. Tu password es: ${info.p}.\nPuedes cambiar tu password en cualquier momento desde tu portal.\nGracias de nuevo y exitos con tu tienda!`,"CCV - Acceso al portal de comerciante aceptado");
      console.log(resultado);
      //Agregar Usuario a la BD
      this.add_user(info.u,nombre_tienda, email, info.p, telefono);
    
    }else{
      const resultado = await this.Alpha.enviarCorreo(email,'Buen dia. La solicitud para agregar tu tienda fue rechazada, revisa tus datos o escribenos a Soporte@CCV.com para mas informacion.',"CCV - Acceso al portal de comerciante denegado");
      console.log(resultado);
    }
    //Actualizacion de estado
    this.validate_solicitud(nombre_tienda, usuario, descrip, email, telefono);
    this.traer_solicitudes();
  }

  //Cambiar de estado la solicitud
  async validate_solicitud(nombre_tienda, usuario, descrip, correo, telefono){
      await this.Alpha.main('update-solicitud', { 
      name_store: nombre_tienda, 
      username: usuario, 
      descripcion: descrip, 
      email: correo, 
      phone: telefono, 
      aceptada: 1 
    } );
  }

  solicitudes = [];
  async traer_solicitudes(){
    this.solicitudes = [];
    this.solicitudes = await this.Alpha.main('get-solicitudes');

  }

  //Agregar Usuario
  async add_user(p_nombre_usuario, p_apellido_usuario, p_correo, p_password, p_telefono){
    await this.Alpha.main('add-userfromadmin', { 
      nombre_usuario: p_nombre_usuario, 
      apellido_usuario: p_apellido_usuario, 
      correo: p_correo, 
      password: p_password, 
      telefono: p_telefono,
      rol: 'comerciante',
      remitente: 'admin'
    } );
  }
    

}
