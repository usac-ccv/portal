import { Component, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-admin-departamentos',
  templateUrl: './admin-departamentos.component.html',
  styleUrls: ['./admin-departamentos.component.css']
})
export class AdminDepartamentosComponent implements OnInit {

  actualizado = false;
  eliminado = false;
  nuevo = false;
  interval = 0;
  opnombre = '';
  actualizando = false;

  constructor(public Alpha:AlphaService) {
    this.getDepartamentos();
  }

  departamentos = [];
  async getDepartamentos(){
    this.departamentos = [];
    this.departamentos = await this.Alpha.main('get-departamentos');
   }

  ngOnInit(): void {
    setInterval(()=>{
      this.interval++;
      this.actualizado = false;
      this.eliminado = false;
      this.nuevo = false;
    }, 10000);
  }

    
  verActualizar(nombre){
    this.actualizando = true;
    this.opnombre = nombre;
  }

  //CRUD DE DEPARTAMENTOS
  async agregar(nombre){
    var resultado = await this.Alpha.main('registrar-departamento',{nombre:nombre});
    this.nuevo = true;
    this.getDepartamentos();
  }

  async actualizar(nuevonom){
    //console.log('actualizar el departamento '+ this.opnombre+' con ' + nuevonom);
    var resultado = await this.Alpha.main('update-departamento',{old:this.opnombre,nuevo:nuevonom});
    //console.log(resultado);
    this.actualizando = false;
    this.actualizado = true;
    this.getDepartamentos();
  }

  async eliminar(nombre){
    this.opnombre = nombre;
    //console.log('eliminar el departamento '+ this.opnombre);
    var resultado = await this.Alpha.main('eliminar-departamento',{nombre:nombre});
    //console.log(resultado);
    this.eliminado = true;
    this.getDepartamentos();
  }

}
