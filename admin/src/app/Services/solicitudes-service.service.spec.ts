import { TestBed } from '@angular/core/testing';

import { servSolicitudes } from './solicitudes-service.service';

describe('servSolicitudes', () => {
  let service: servSolicitudes;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(servSolicitudes);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
