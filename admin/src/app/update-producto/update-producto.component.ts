import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlphaService } from '../Services/alpha.service';
import { DataProdcService } from '../Services/data-prodc.service';
import { Product } from '../models/Product';

@Component({
  selector: 'app-update-producto',
  templateUrl: './update-producto.component.html',
  styleUrls: ['./update-producto.component.css']
})
export class UpdateProductoComponent implements OnInit {

  public image: string;
  public id: number;

  constructor(public router: Router, public Alpha: AlphaService, public get_dataprod:DataProdcService) {
  }

  ngOnInit(): void {
    this.productos = this.get_dataprod.product;
    this.departamentos = this.get_dataprod.departamentos;
    this.image = this.productos.imagen;
    this.search();
    this.getDepartamentos();
  }

  productos: Product = {
    id: 0,
    nombre: '',
    imagen: '', //image url 
    cantidad: 0,
    precio: 0
  };

  categorias: [];
  tiendas = [];
  departamentos = [];
  seleccionadas = [];
  d_seleccionadas;

  async search(){
    this.categorias = await this.Alpha.main("get-categorias");
    this.Alpha.productos = await this.Alpha.main("search", { keyword: "" })
    console.log(this.categorias);
    console.log(this.departamentos);
    console.log(this.Alpha.productos);
  }

  async getDepartamentos(){
    this.departamentos = [];
    this.departamentos = await this.Alpha.main('get-departamentos')
  }

  selected: string = ' ';

 selectOption(id: string) {
   this.d_seleccionadas = id;
 }

  checkCheckBoxvalue(categoria){   
    this.seleccionadas.push(categoria);
  }
  checkCheckRadevalue(departamentos){
    this.d_seleccionadas = departamentos;
  }

  update_producto(){
    console.log("seleccionados",this.seleccionadas);
    console.log('envio=====>'+this.id)
    this.Alpha.main("update-producto", {  
      id: this.productos.id, 
      nombre: this.productos.nombre, 
      cantidad: this.productos.cantidad, 
      precio: this.productos.precio,
      categorias: this.seleccionadas,
      departamento: this.d_seleccionadas,
      imagen: this.id
    });
    this.salir();
  }

  salir(){
    this.router.navigate(['']);
  }

  updateProducto(p: Product){
    console.log('Producto seleccionado: '+JSON.stringify(p));
    this.productos = p;
    this.image = p.imagen;
    this.get_dataprod.product = p;
  }

  refresh($event: {id:number, url:string}){
    this.id = $event.id;
    this.image = $event.url;
    this.productos.imagen = $event.url;
    console.log($event);
  }

  async eliminarProducto(id: number){
    await this.Alpha.main("eliminar-producto", { id: id });
    this.search();
  }
}
