import { Component, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-admin-categorias',
  templateUrl: './admin-categorias.component.html',
  styleUrls: ['./admin-categorias.component.css']
})
export class AdminCategoriasComponent implements OnInit {

  actualizado = false;
  eliminado = false;
  interval = 0;
  opnombre = '';
  actualizando = false;

  constructor(public Alpha:AlphaService) {
    this.getCategorias();
  }

  categorias = [];
  async getCategorias(){
    this.categorias = [];
    this.categorias = await this.Alpha.main('get-producto-categorias');
    console.log(this.categorias);
  }

  //categorias = [
    //{num: 1, nombre:'Gafas de sol'},
    //{num: 2, nombre:'Relojes'},
    //{num: 3, nombre:'Cinturones'},
   // {num: 4, nombre:'Camisas'},
    //{num: 5, nombre:'Billeteras'}]

  
  ngOnInit(): void {
    setInterval(()=>{
      this.interval++;
      this.actualizado = false;
      this.eliminado = false;
    }, 5000);
  }

  
  
  verActualizar(nombre){
    this.actualizando = true;
    this.opnombre = nombre;
  }

  //CRUD DE CATEGORIAS
  async agregar(nombre){
    var resultado = await this.Alpha.main('registrar-categoria',{nombre:nombre});
    console.log(resultado);
  }

  async actualizar(nuevonom){
    console.log('actualizar la categoria '+ this.opnombre+' con ' + nuevonom);
    var resultado = await this.Alpha.main('update-categoria',{cambiar:this.opnombre,con:nuevonom});
    console.log(resultado);
    this.actualizando = false;
    this.actualizado = true;
  }

  async eliminar(nombre){
    this.opnombre = nombre;
    console.log('eliminar la categoria '+ this.opnombre);
    var resultado = await this.Alpha.main('eliminar-categoria',{eliminar:nombre});
    console.log(resultado);
    this.eliminado = true;
  }

}
