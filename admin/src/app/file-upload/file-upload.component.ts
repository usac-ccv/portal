import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { HttpClient, HttpResponse, HttpRequest,HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { of } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';
import {AlphaService} from '../Services/alpha.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
  animations: [
        trigger('fadeInOut', [
              state('in', style({ opacity: 100 })),
              transition('* => void', [
                    animate(300, style({ opacity: 0 }))
              ])
        ])
  ]
})
export class FileUploadComponent implements OnInit {

  file_name : string = '';

  /* 
    Ejemplo de un allowed types variado. En este caso solo nos interesa imagen.

    allowed_types = {
      text: /text\/.+/,
      image: /image\/.+/,
      document : /application\/pdf/,
      audio : /audio\/.+/,
      video : /video\/.+/,
      compressed : /application\/((gzip)|(zip)|(jar)|(rar)|(egg))/
    };
  */

  allowed_types = { image: /image\/.+/ };

  verify_required_fields():boolean {
    if(this.files.length != 0){
      this.Alpha.error('You can only upload one file at a time.');
      return false;
    }
    return true;
  }
      /** Link text */
      @Input() text = 'Upload';
      @Input() imagen : string = null;
      @Input() accept = '*/*';

      @Output() complete = new EventEmitter<{id: number, url: string}>(); //Se ejecuta al finalizar la subida de la imagen. En este caso solo regresa
      //el id de la nueva imagen.

      public files: Array<FileUploadModel> = [];

      constructor(private _http: HttpClient, public Alpha:AlphaService) { }

      ngOnInit() {}

      onClick() {
        if(!this.verify_required_fields()) return; //Check only one file is in queue.
            const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
            fileUpload.onchange = () => {
                  for (let index = 0; index < fileUpload.files.length; index++) {
                        const file = fileUpload.files[index];
                        this.files.push({ data: file, state: 'in',
                          inProgress: false, progress: 0, canRetry: false, canCancel: false });
                  }
                  this.uploadFiles();
            };
            fileUpload.click();
      }

      cancelFile(file: FileUploadModel) {
            file.sub.unsubscribe();
            this.removeFileFromArray(file);
      }

      retryFile(file: FileUploadModel) {
            this.uploadFile(file);
            file.canRetry = false;
      }

      private async uploadFile(file: FileUploadModel) {
        let fname = file.data.name;
        let size = file.data.size; //In bytes
        let ftype = file.data.type; //True File Type
        let jm : string; //File type
        const forbidden_chars = /[^0-9a-zA-Z!_.()\s-]+/g;
        const re = /(?:\.([^.]+))?$/;
        this.file_name = fname.replace(forbidden_chars,'-').replace(/\s+/g,'_');
        const extension = re.exec(this.file_name)[1];
        if(!extension){
          this.Alpha.error('For security reasons it is forbidden to upload extension-less files.');
          this.removeFileFromArray(file);
          return;
        }
        this.file_name = this.file_name.substring(0,this.file_name.length-extension.length-1); 
        for(const [a,b] of Object.entries(this.allowed_types)){
          if (b.test(ftype)){ jm = a; break;}
        }
        if(!jm){
          this.Alpha.error(`For security reasons, ${ftype} files are forbidden.`);
          this.removeFileFromArray(file);
          return;
        }
        const fbody = {
          nombre : this.file_name,
          tipo : extension,
          size : size
        };
        //Perform http request to get presigned url.
        let ren : object = await this.Alpha.registerFile(fbody);
        let url : string = ren['url'];
        let key : string = ren['key'];
        let rid : number = Number(ren['id']);
        if (!url){
          this.Alpha.error('An error ocurred while uploaing the file. Please, try again later.');
          this.removeFileFromArray(file);
          return;
        }
          const req = new HttpRequest('PUT', url, file.data, {
                  reportProgress: true
          });

            file.inProgress = true;
            file.sub = this._http.request(req).pipe(
                  map(event => {
                        switch (event.type) {
                              case HttpEventType.UploadProgress:
                                    file.progress = Math.round(event.loaded * 100 / event.total);
                                    break;
                              case HttpEventType.Response:
                                    return event;
                        }
                  }),
                  tap(message => { }),
                  last(),
                  catchError((error: HttpErrorResponse) => {
                        file.inProgress = false;
                        file.canRetry = true;
                        return of(`${file.data.name} upload failed.`);
                  })
            ).subscribe(
                  (event: any) => {
                        if (typeof (event) === 'object') {
                              //Subida finalizada con exito!
                              this.file_name = '';
                              const sg = 'https://ccv-assets.s3.us-east-2.amazonaws.com/'+key;
                              this.removeFileFromArray(file);
                              this.complete.emit({id: rid, url: sg});
                        }
                  }
            );
      }

      private uploadFiles() {
            const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
            fileUpload.value = '';

            this.files.forEach(file => {
                  this.uploadFile(file);
            });
      }

      private removeFileFromArray(file: FileUploadModel) {
            const index = this.files.indexOf(file);
            if (index > -1) {
                  this.files.splice(index, 1);
            }
      }

}

export class FileUploadModel {
      data: File;
      state: string;
      inProgress: boolean;
      progress: number;
      canRetry: boolean;
      canCancel: boolean;
      sub?: Subscription;
}