import { Component } from '@angular/core';
import { AlphaService } from './Services/alpha.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'admin';

  constructor(public Alpha:AlphaService){
    //Recompile admin module
    this.reload();
  }
  
  async reload(){
    const initial_data = await this.Alpha.LoadHome(window.location.search);
    console.log(initial_data);
  }
}
