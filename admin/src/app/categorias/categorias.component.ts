import { Component, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {
  
  actualizado = false;
  nuevo = false;
  eliminado = false;
  opnombre = '';
  actualizando = false;

  constructor(public Alpha:AlphaService) { 
    this.getCategorias();
  }

  ngOnInit(): void {
    setInterval(()=>{
      this.actualizado = false;
      this.eliminado = false;
      this.nuevo = false;
    }, 10000);
  }

  categorias = [];
  async getCategorias(){
    this.categorias = [];
    this.categorias = await this.Alpha.main('get-categorias');
    console.log(this.categorias);
  }

    
  verActualizar(nombre){
    this.actualizando = true;
    this.opnombre = nombre;
  }

  //CRUD DE CATEGORIAS
  async agregar(nombre){
    var resultado = await this.Alpha.main('registrar-categoria',{nombre:nombre});
    this.nuevo = true;
    this.getCategorias();
    console.log(resultado);
  }

  async actualizar(nuevonom){
    console.log('actualizar la categoria '+ this.opnombre+' con ' + nuevonom);
    var resultado = await this.Alpha.main('update-categoria',{old:this.opnombre,nuevo:nuevonom});
    console.log(resultado);
    this.getCategorias();
    this.actualizando = false;
    this.actualizado = true;
  }

  async eliminar(nombre){
    this.opnombre = nombre;
    console.log('eliminar la categoria '+ this.opnombre);
    var resultado = await this.Alpha.main('eliminar-categoria',{nombre:nombre});
    console.log(resultado);
    this.eliminado = true;
    this.getCategorias();
  }

}
