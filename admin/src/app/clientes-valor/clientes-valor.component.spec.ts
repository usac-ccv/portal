import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesValorComponent } from './clientes-valor.component';

describe('ClientesValorComponent', () => {
  let component: ClientesValorComponent;
  let fixture: ComponentFixture<ClientesValorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesValorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesValorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
