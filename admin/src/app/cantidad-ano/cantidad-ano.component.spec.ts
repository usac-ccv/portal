import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CantidadAnoComponent } from './cantidad-ano.component';

describe('CantidadAnoComponent', () => {
  let component: CantidadAnoComponent;
  let fixture: ComponentFixture<CantidadAnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CantidadAnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CantidadAnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
