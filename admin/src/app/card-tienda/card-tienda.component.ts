import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Tienda } from '../models/Tienda'

@Component({
  selector: 'app-card-tienda',
  templateUrl: './card-tienda.component.html',
  styleUrls: ['./card-tienda.component.css']
})
export class CardTiendaComponent implements OnInit {

  @Input() tienda : Tienda;

  constructor() {}

  ngOnInit(): void {} 

}
