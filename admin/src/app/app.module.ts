import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TopNavBarComponent } from './top-nav-bar/top-nav-bar.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';
import { AdministracionComponent } from './administracion/administracion.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { AdminTiendasComponent } from './admin-tiendas/admin-tiendas.component';
import { AdminDepartamentosComponent } from './admin-departamentos/admin-departamentos.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { FormsModule } from "@angular/forms";
import { CategoriasComponent } from './categorias/categorias.component';
import { UpdateProductoComponent } from './update-producto/update-producto.component';
import { GridComponent } from './grid/grid.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { TiendaDisplayComponent } from './tienda-display/tienda-display.component';
import { CardTiendaComponent } from './card-tienda/card-tienda.component';
import { ReportesComponent } from './reportes/reportes.component';
import { ChartsModule } from 'ng2-charts';
import { TiendaMayorVentaComponent } from './tienda-mayor-venta/tienda-mayor-venta.component';
import { ProductosVendidosComponent } from './productos-vendidos/productos-vendidos.component';
import { ClientesValorComponent } from './clientes-valor/clientes-valor.component';
import { ClientesCantidadComponent } from './clientes-cantidad/clientes-cantidad.component';
import { CantidadAnoComponent } from './cantidad-ano/cantidad-ano.component';
import { CantidadMesComponent } from './cantidad-mes/cantidad-mes.component';

@NgModule({
  declarations: [
    AppComponent,
    TopNavBarComponent,
    NotificacionesComponent,
    AdministracionComponent,
    PedidosComponent,
    AdminTiendasComponent,
    AdminDepartamentosComponent,
    SearchbarComponent,
    CategoriasComponent,
    UpdateProductoComponent,
    GridComponent,
    FileUploadComponent,
    CardTiendaComponent,
    TiendaDisplayComponent,
    ReportesComponent,
    TiendaMayorVentaComponent,
    ProductosVendidosComponent,
    ClientesValorComponent,
    ClientesCantidadComponent,
    CantidadAnoComponent,
    CantidadMesComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
