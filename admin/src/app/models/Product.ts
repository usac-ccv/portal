export class Product {
      id: number;
      nombre: string;
      cantidad: number;
      imagen: string;
      precio: number;
      categorias?: string[];
      departamento?: string;
      tienda?: number;
}
