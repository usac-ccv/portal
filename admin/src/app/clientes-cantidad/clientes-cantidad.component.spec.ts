import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesCantidadComponent } from './clientes-cantidad.component';

describe('ClientesCantidadComponent', () => {
  let component: ClientesCantidadComponent;
  let fixture: ComponentFixture<ClientesCantidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesCantidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesCantidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
