import { Component, Input, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  constructor( public Alpha:AlphaService) { }

  ngOnInit(): void {
  }

  campo_busqueda = "";
  
  async search(){
    this.Alpha.productos = await this.Alpha.main('search', { 
      keyword: this.campo_busqueda
    } );
    this.clean_searchbar();
  }

  clean_searchbar(){
    this.campo_busqueda = "";
  }
}
