import { Component, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  enviado = false;
  error = false;
  number = 0;

  constructor(public Alpha: AlphaService) {
    this.getPedidos();
   }

  ngOnInit(): void {
    setInterval(()=>{
      this.enviado = false;
      this.error = false;
    },30000);
  }

  pedidos = [];
  historial = []

  async getPedidos(){
    this.pedidos = [];
    this.historial = [];
    var todos = await this.Alpha.main('get-pedidos');
    for (let index = 0; index < todos.length; index++) {
      const element = todos[index];
      if(!element.aprobado){
        this.pedidos.push(element);
      }else{
        this.historial.push(element);
      }
      
    }
  }

  async confirmar(id,nombre, email, productos){
    let mensaje = 'Tu pedido '+ nombre +' ha sido entregado exitosamente \n';
    mensaje += 'Factura\n';
    for (let index = 0;  index < productos.length; index++) {
    const element = productos[index];
    mensaje += 'Producto: '+element.producto+'\n';
    mensaje += 'Cantidad: '+element.cantidad+'\n';
    mensaje += 'Precio unitario: '+ element.precio+'\n';
    mensaje += 'Subtotal:' + element.precio*element.cantidad +'\n';
    mensaje += '------------------------------------------------------\n';
    }
    mensaje += '\n Gracias por comprar en el CCV!!\n'
    let asunto = 'CCV: Confirmacion de pedido';
    const resultado = await this.Alpha.enviarCorreo(email,mensaje,asunto);
    if(resultado == 'email queued successfully'){
      this.enviado = true;
      this.actualizar(id);
    }else{
      this.error = true;
    }
  }


  async actualizar(id){
    const resultado = await this.Alpha.main('aprobar-pedido',{pedido:id});
    console.log(resultado);
    this.getPedidos();
  }
}
