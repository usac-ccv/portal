import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiendaMayorVentaComponent } from './tienda-mayor-venta.component';

describe('TiendaMayorVentaComponent', () => {
  let component: TiendaMayorVentaComponent;
  let fixture: ComponentFixture<TiendaMayorVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiendaMayorVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiendaMayorVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
