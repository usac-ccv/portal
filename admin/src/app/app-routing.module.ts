import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { AdministracionComponent } from './administracion/administracion.component';
import { AdminTiendasComponent } from './admin-tiendas/admin-tiendas.component';
import { AdminDepartamentosComponent } from './admin-departamentos/admin-departamentos.component';
import { AdminCategoriasComponent } from './admin-categorias/admin-categorias.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { UpdateProductoComponent } from './update-producto/update-producto.component';
import { TiendaDisplayComponent } from './tienda-display/tienda-display.component';
import { ReportesComponent } from './reportes/reportes.component';
import { TiendaMayorVentaComponent } from './tienda-mayor-venta/tienda-mayor-venta.component';
import { ProductosVendidosComponent } from './productos-vendidos/productos-vendidos.component';
import { ClientesValorComponent } from './clientes-valor/clientes-valor.component';
import { ClientesCantidadComponent } from './clientes-cantidad/clientes-cantidad.component';
import { CantidadAnoComponent } from './cantidad-ano/cantidad-ano.component';
import { CantidadMesComponent } from './cantidad-mes/cantidad-mes.component';


const routes: Routes = [
  {path: 'administracion', component: AdministracionComponent},
  {path: 'notificaciones', component: NotificacionesComponent},
  {path: 'pedidos', component: PedidosComponent},
  {path: 'tiendas', component: AdminTiendasComponent},
  {path: 'departamentos', component: AdminDepartamentosComponent},
  {path: 'vercategorias', component: AdminCategoriasComponent},
  {path: 'index.html', component:AdministracionComponent},
  {path: 'categorias', component: CategoriasComponent},
  {path: 'update-prod', component: UpdateProductoComponent},
  {path: 'tienda-display', component: TiendaDisplayComponent},
  {path: 'reportes', component: ReportesComponent},
  {path: 'tienda-mayor-venta', component: TiendaMayorVentaComponent},
  {path: 'productos-vendidos', component: ProductosVendidosComponent},
  {path: 'clientes-valor', component: ClientesValorComponent},
  {path: 'clientes-cantidad', component: ClientesCantidadComponent},
  {path: 'cantidad-ano', component: CantidadAnoComponent},
  {path: 'cantidad-mes', component: CantidadMesComponent},
  {path: '', component:AdministracionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
