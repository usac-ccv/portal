import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FormsModule } from "@angular/forms";
import { SearchbarComponent } from './searchbar/searchbar.component';
import { InicioUsuarioComponent } from './inicio-usuario/inicio-usuario.component';
import { GridComponent } from './grid/grid.component';
import { CardTiendaComponent } from './card-tienda/card-tienda.component';
import { TiendaDisplayComponent } from './tienda-display/tienda-display.component';
import { FormularioPagoComponent } from './formulario-pago/formulario-pago.component';
import { CarritoComponent } from './carrito/carrito.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioUsuarioComponent,
    SearchbarComponent,
    CardTiendaComponent,
    TiendaDisplayComponent,
    GridComponent,
    FormularioPagoComponent,
    CarritoComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
