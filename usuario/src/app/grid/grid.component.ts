import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { AlphaService } from '../Services/alpha.service';
import { Tienda } from '../models/Tienda'
import { Product } from '../models/Product'

@Component({
  selector: 'app-grid-component',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

      @Input() productos : Array<Tienda> = [];
      @Input() tiendas : Array<Product> = [];
      @Input() is_admin : boolean = true;

      @Output() product_edit = new EventEmitter<Product>();
      @Output() product_delete = new EventEmitter<number>();
      @Output() product_cart = new EventEmitter<{id: number, cantidad: number}>();
      @Output() tienda_select = new EventEmitter<string>();

      cantidades: any;

      constructor(public Alpha: AlphaService, public router: Router) {
        this.cantidades = {};
      } 

      ngOnInit(): void {
        this.cantidades = {};
      }

      forward_product_edit(product: Product){
            this.product_edit.emit(product);
      }
      forward_product_delete(id: number){
            this.product_delete.emit(id);
      }
      forward_product_cart(product: Product){
            let cantidad = this.cantidades[product.id];
            if( cantidad == 0 ){
              alert('No se puede agregar 0 productos al carrito');
              this.cantidades = {};
              return;
            }else if( cantidad > product.cantidad ){
              alert('No hay tantas copias en existencia. El maximo actual es: '+product.cantidad);
              this.cantidades = {};
              return;
            }else if ( cantidad <= 0 ){
              alert('No se aceptan cantidades negativas.');
              this.cantidades = {};
              return;
            }
            this.product_cart.emit( {id: product.id, cantidad: cantidad } );
            this.cantidades = {};
      }
      forward_tienda_select(nombre: string){
            this.tienda_select.emit(nombre);
      }

      selected2: string = "";
      
      selectOption2(id: string){
        //console.log(id);
        this.search(id);
      }

      clean_searchbar(){
        this.selected2 = "";
      }
    
      options = [];
      async verDeps(){
        this.options = await this.Alpha.main('get-departamentos');
        
      }
    
      async verCats(){
        this.options = await this.Alpha.main('get-categorias');
      }
    

      async search(key){
        this.Alpha.productos = await this.Alpha.main('search', { 
          keyword: key
        } );
        this.clean_searchbar();
      }
}
