import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../models/Product';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  constructor(public Alpha: AlphaService, public router: Router) { }

  ngOnInit(): void {
    //this.productos = this.Alpha.producto_seleccionado;
    this.obtenerproduc();
  }

  productos:Product[]=[];
  
  modificar_cantidad(data_prod){
    let modificado = false;
    // Actualizar cantidad del producto
    let cant_max = this.Obtener_cantidadmaxima(data_prod.id);
    if( data_prod.cantidad == 0 ){
      alert('No se puede modificar la cantidad a 0.');
    }else if ( data_prod.cantidad <= 0 ){
      alert('No se aceptan cantidades negativas.');
    }else if( data_prod.cantidad > cant_max ){
      alert('No hay tantas copias en existencia. El maximo actual es: '+cant_max);
    }else{
      modificado = true;
    }
    
    if(modificado){
      this.Alpha.main("actualizar-cantidad-producto", {
        producto: data_prod.id,
        cantidad: data_prod.cantidad
      });
      alert('Modificado Correctamente');
    }else{
      //Actualizar campoa
      this.obtenerproduc();
      this.ngOnInit();
    }
  }

  eliminar_producto(data_prod){
    this.Alpha.main("borrar-producto", {producto: data_prod.id});
    this.obtenerproduc();
    this.ngOnInit();
  }

  async obtenerproduc(){
    this.productos = await this.Alpha.main('get-cart');
  }

  async limpiar_carrito(){
    this.productos = await this.Alpha.main('limpiar-carrito');
    this.router.navigate(['']);
  }

  Obtener_cantidadmaxima(id_producto):number{
    let result:number;
    for(let n_product of this.Alpha.productos){
      if(n_product.id == id_producto){
        return n_product.cantidad;
      }
    }
    return result;
  }
}
