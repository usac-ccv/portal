const baseUrl = 'https://h6li2mrz2i.execute-api.us-east-2.amazonaws.com/beta';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/Product';
import { Tienda } from '../models/Tienda';

@Injectable({
  providedIn: 'root'
})
export class AlphaService {
  private token;

  /* Variables used for inter-component communication */
  public productos: Array<Product>;
  public tienda: Tienda;
  /* -----------------------------------------------  */

  /* ------------------------------------------------ */
  //public producto_seleccionado = [];
  /* ------------------------------------------------ */

  constructor(private http: HttpClient) {
    this.productos = []; 
    this.tienda = null;
  }

  private request(method: string, url: string, data?: any,response_type:"json"|"text"|"arraybuffer" = 'json') {
    const result = this.http.request(method, url, {
      body: data,
      responseType: response_type,
      observe: 'body'
    });
    return new Promise<any>((resolve, reject) => {
      result.subscribe(resolve as any, reject as any);
    });
  }

  public async ReloadTienda(){
      if(!this.tienda) return;
      if(this.tienda.nombre) this.tienda = await this.main('get-tienda',{nombre: this.tienda.nombre});
      else if(this.tienda.id) this.tienda = await this.main('get-tienda',{id: this.tienda.id});
      else return;
      this.productos = this.tienda.productos;
  }
  
  logout(){
    this.request('get',`${baseUrl}/logout/${this.token}`).then(()=>{
      this.token = 0;
    });
  }
  
  public async LoadHome(code?){
    if(!code){
      if(this.token){
        if(this.token > 0){
          return await this.main('get-home',{module:'user'});
        }
      }
    }
    code = new URLSearchParams(code);
    code = code.get('token')
    code = this.decode(code,'|');
    if(!code){
      window.location.href='https://usac-ccv.gitlab.io/portal';
      return;
    }
    this.token = code.token;
    console.log(code);
    return await this.main('get-home',{module:'user'});
  }

  public main(tipo: string, data?: object): any {
    let req = {
        token : this.token,
        tipo: tipo,
        data: data
    };
    
    return new Promise((resolve,reject)=>{
      this.request('post', `${baseUrl}/main`,req).then((v)=>resolve(v)).catch((err)=>{
        console.log(err);
        reject({tipo:tipo,data:data,error:err});
      })
    });
  }

  resultadoSolicitud(correo,mensaje,subject){
    let post = {
      token:Number(this.token),
      to: correo,
      body: mensaje,
      subject: subject
    };
    return this.request('post',`${baseUrl}/correo`,post);
  }

  /** Utils **/
  private decode (str,key){
    str = decodeURIComponent(decodeURIComponent(str)
        .split(key).map(
                c=>String.fromCharCode(Number(c))
        ).join(''));
    try { return JSON.parse(str) }
    catch (err) {
      return null;
    }
  }

}
