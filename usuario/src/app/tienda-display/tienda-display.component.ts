import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { AlphaService } from '../Services/alpha.service';

@Component({
  selector: 'app-tienda-display',
  templateUrl: './tienda-display.component.html',
  styleUrls: ['./tienda-display.component.css']
})
export class TiendaDisplayComponent implements OnInit {

  constructor(public Alpha: AlphaService) {}

  ngOnInit(): void {
    this.Alpha.ReloadTienda();
  }

}
