import { Component, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service';
import { Router } from '@angular/router';
import { Tienda } from '../models/Tienda';

@Component({
  selector: 'app-inicio-usuario',
  templateUrl: './inicio-usuario.component.html',
  styleUrls: ['./inicio-usuario.component.css']
})
export class InicioUsuarioComponent implements OnInit {

  tiendas: Array<Tienda>;

  constructor(public Alpha: AlphaService,private router: Router) { }

  ngOnInit(): void {
    this.tiendas=[];
    this.LoadHome();
  }

  async LoadHome(){
    const home = await this.Alpha.LoadHome();
    this.Alpha.productos = home.productos;
    this.tiendas = home.tiendas;
  }

  select_tienda(nombre: string){
    this.Alpha.tienda = new Tienda();
    this.Alpha.tienda.nombre = nombre;
    this.router.navigate(['display-tienda']);
  }

  async add_product_to_cart(seleccionado){
    //console.log(seleccionado);
    let asignados = await this.Alpha.main('get-cart');

    for(let nodo of asignados){
      if(nodo.id == seleccionado.id){
        return;
      }
    }

    this.Alpha.main("add-to-cart", {
      producto: seleccionado.id,
      cantidad: seleccionado.cantidad
    });
    //Notificaciones y actualizacion
    alert("Producto Agregado Correctamente");
  }

}
