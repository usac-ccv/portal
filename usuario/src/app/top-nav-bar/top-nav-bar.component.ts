import { Component, OnInit } from '@angular/core';
import { AlphaService } from '../Services/alpha.service'
import { SearchbarComponent } from '../searchbar/searchbar.component';

@Component({
  selector: 'app-top-nav-bar',
  templateUrl: './top-nav-bar.component.html',
  styleUrls: ['./top-nav-bar.component.css']
})

export class TopNavBarComponent implements OnInit {

  constructor(public Alpha:AlphaService) {
    this.traer_solicitudes();
  }
  public numNotificaciones = 0;
  public numPedidos = 0;
  ngOnInit(): void { }

  //Notificaciones
  async traer_solicitudes(){
    let solicitudes = [];
    solicitudes = await this.Alpha.main('get-solicitudes');
    this.numNotificaciones = solicitudes.length;
  }

  //Limpiar Notificaciones
  limpiarnoti(){
    this.numNotificaciones = 0;
  }

  async goHome(){
    const home = await this.Alpha.LoadHome();
    this.Alpha.productos = home.productos;
  }

  cerrarSesion(){
      window.location.href='https://usac-ccv.gitlab.io/portal';
      return;
  }
}
