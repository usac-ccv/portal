import { Component, OnInit } from '@angular/core';
import { Product } from '../models/Product';
import { AlphaService } from '../Services/alpha.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario-pago',
  templateUrl: './formulario-pago.component.html',
  styleUrls: ['./formulario-pago.component.css']
})
export class FormularioPagoComponent implements OnInit {

  constructor(public Alpha: AlphaService, public router: Router) { }

  confirmado = false;

  ngOnInit(): void {
    setInterval(()=>{
      this.redirigir();
      this.confirmado = false;
    },10000);
    this.getCart();
  }

  productos:Product[] = []

  async getCart(){
    this.productos = await this.Alpha.main('get-cart');
    this.calcular();
  }

  total = 0;

  calcular(){
    for (let index = 0; index < this.productos.length; index++) {
      const element = this.productos[index];
      this.total += (element.cantidad*element.precio);
    }
  }

  async confirmar(nombre,apellido,dpi,email){
    const result = this.Alpha.main('confirmar-orden',{nombre:nombre,dpi:dpi});
    this.confirmado = true;
    
  }

  redirigir(){
    if(this.confirmado){
      this.limpiar_carrito();
      this.router.navigate(['']);
    }
  }

  async limpiar_carrito(){
    this.productos = await this.Alpha.main('limpiar-carrito');
  }
}
