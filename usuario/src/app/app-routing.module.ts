import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarritoComponent } from './carrito/carrito.component';
import { InicioUsuarioComponent } from './inicio-usuario/inicio-usuario.component';
import { TiendaDisplayComponent } from './tienda-display/tienda-display.component';
import { FormularioPagoComponent } from '../app/formulario-pago/formulario-pago.component'

const routes: Routes = [
  {path: 'index.html', component: InicioUsuarioComponent},
  {path: 'tienda-display', component: TiendaDisplayComponent},
  {path: 'display-tienda', component: TiendaDisplayComponent},
  {path: 'formulario',component:FormularioPagoComponent},
  {path: 'carrito', component: CarritoComponent},
  {path: '', component: InicioUsuarioComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
